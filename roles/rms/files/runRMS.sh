#!/bin/bash

# Clean old run
/bin/rm -rf /home/pi/RMS_data

# Make new data dir 
/usr/bin/mkdir -p /home/pi/RMS_data

# Patch problem with lib
export LD_PRELOAD=/usr/lib/arm-linux-gnueabihf/libatomic.so.1 

source /home/pi/vRMS/bin/activate
cd /home/pi/source/RMS-master

# Prevent RMS for imidiate reboot
touch /home/pi/RMS_data/.reboot_lock

# Run RMS (it will wait for the next run)
/home/pi/vRMS/bin/python -m RMS.StartCapture

# Backup
if mount | grep /mnt > /dev/null 
then
 BACKUP_DIR=/mnt/rms-north/`date +%Y-%m-%d`
 mkdir -p ${BACKUP_DIR}
 mv ~/RMS_data ${BACKUP_DIR}
fi

# Remove lockfile and let the system reboot itself
rm -f /home/pi/RMS_data/.reboot_lock
