#!/bin/bash

sudo apt update
sudo apt install -y ansible

cd
rm -rf ~/source
mkdir ~/source
cd ~/source
curl https://gitlab.com/rms-dome/compute-nodes/-/archive/master/compute-nodes-master.tar.bz2 | tar xvj 
cd ~/source/compute-nodes-master/playbooks
ansible-playbook rms.yaml
